<?php

use Illuminate\Database\Seeder;
use App\Collegue;

class ColleguesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Collegue::create(
            [
                'name'=>'Tom Rensed',
                'positions'=>'Chief Executive Officer',
                'text'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin consequat sollicitudin cursus. Dolor sit amet, consectetur adipiscing elit proin consequat.',
                'images'=>'team_pic1.jpg'
            ]
        );
        Collegue::create(
            [
                'name'=>'Kathren Mory',
                'positions'=>'Vice President',
                'text'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin consequat sollicitudin cursus. Dolor sit amet, consectetur adipiscing elit proin consequat.',
                'images'=>'team_pic2.jpg'
            ]
        );
        Collegue::create(
            [
                'name'=>'Lancer Jack',
                'positions'=>'Senior Manager',
                'text'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin consequat sollicitudin cursus. Dolor sit amet, consectetur adipiscing elit proin consequat.',
                'images'=>'team_pic3.jpg'
            ]
        );
    }
}
