<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ColleguesTableSeeder::class);
        $this->call(PagesTableSeeder::class);
        $this->call(PortfoliosTableSeeder::class);
        $this->call(ServicesTableSeeder::class);
    }
}
